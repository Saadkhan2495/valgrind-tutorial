# Valgrind Tutorial

## Build Application
To build the main application used the following commands:

```bash
mkdir build
cd build
cmake ..
make
```
The main executable is named `valgrind-tutorial`.

To build the examples as well, add the flag `BUILD_EXAMPLES=True` to the cmake command.

```bash
cmake -DBUILD_EXAMPLES=True ..
```
Executables of the examples are available in directory `build/e_g-executables`.

___Note:__ By defaut, the application is built in Debug mode since Release mode is usually not preferred for valgrind._