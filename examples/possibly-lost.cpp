#include <iostream>

void *int_array;

void my_function(void)
{
   int_array             = new int[10];
   ((int *)int_array)[5] = 666;
   ((int *)int_array)[0] = 111;
   int_array             = ((int *)int_array) + 1;
   std::cout << "Array Value at index 0: " << ((int *)int_array)[-1] << std::endl;
   std::cout << "Array Value at index 5: " << ((int *)int_array)[4] << std::endl;
}

int main(void)
{
   my_function();
   return 0;
}