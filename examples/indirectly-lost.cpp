class node
{
 public:
   // ~node()
   // {
   //    delete child_1;
   //    delete child_2;
   // }
   void init_children()
   {
      child_1 = new node;
      child_2 = new node;
   }

 private:
   node *child_1;
   node *child_2;
};

void my_function(void)
{
   node *my_node = new node();
   my_node->init_children();
}

int main(void)
{
   my_function();
   return 0;
}