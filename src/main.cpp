#include "student.h"
#include <array>
#include <iostream>
#include <stdlib.h>

uint *student::student_count = new uint(0);

int main()
{
   std::array<std::string, 6> first_names({"Bruce", "Diana", "Clark", "Tony", "Steve", "Natasha"});
   std::array<std::string, 6> last_name({"Wayne", "Prince", "Kent", "Stark", "Rogers", "Romanoff"});

   uint        random_number_1, random_number_2;
   std::string student_name;
   student *   students[5];

   for (uint iterator = 0; iterator < 5; ++iterator)
   {
      random_number_1 = rand() % 6 + 1;
      random_number_2 = rand() % 6 + 1;
      student_name    = first_names[random_number_1 - 1] + " " +
                     last_name[random_number_2 - 1];  // Randomly generate name using the arrays first_names and second_names

      std::cout << "Student name: " << student_name << std::endl;

      students[iterator] = new student(student_name, random_number_1);
      students[iterator]->total_instances();
   }
   return 0;
}