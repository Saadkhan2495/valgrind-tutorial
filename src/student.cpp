#include "student.h"
#include <cstring>
#include <iostream>

void *prev_student_last_name;  // Only used for printing the last name of the last student object created;

student::student(const std::string &full_name, const uint &grade)
   : _full_name(full_name)
{
   _grade         = new uint(grade);
   _subject_marks = new uint[5];

   prev_student_last_name = new char[_full_name.size() + 1];
   std::strcpy((char *)prev_student_last_name, full_name.c_str());

   size_t index = _full_name.find_first_of(" ");
   if (index != std::string::npos)
   {
      prev_student_last_name = (char *)prev_student_last_name + index + 1;
      std::cout << "The last name of the student is: " << (char *)prev_student_last_name << std::endl;
   }

   *student_count += 1;
}

student::~student()
{
   *student_count -= 1;
}

void student::total_instances()
{
   std::cout << "Total students: " << *student_count << std::endl;
}