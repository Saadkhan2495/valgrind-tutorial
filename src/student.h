#ifndef STUDENT_H
#define STUDENT_H

#include <string>

class student
{
 public:
   explicit student(const std::string &full_name, const uint &grade);
   ~student();
   void         total_instances();
   static uint *student_count;  // Keep track of total number of student objects created

 private:
   std::string _full_name;
   uint *      _grade;
   uint *      _subject_marks;
};

#endif